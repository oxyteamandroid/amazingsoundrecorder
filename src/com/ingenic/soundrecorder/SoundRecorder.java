/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ingenic.soundrecorder;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.StatFs;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.ingenic.iwds.HardwareList;
import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.iwds.widget.AmazingToast;
import com.ingenic.iwds.widget.RightScrollView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Calculates remaining recording time based on available disk space and
 * optionally a maximum recording file size.
 * 
 * The reason why this is not trivial is that the file grows in blocks every few
 * seconds or so, while we want a smooth countdown.
 */

class RemainingTimeCalculator {
	public static final String TAG = "RemainingTimeCalculator";

	private static final boolean DEB = false;

	public static final int UNKNOWN_LIMIT = 0;
	public static final int FILE_SIZE_LIMIT = 1;
	public static final int DISK_SPACE_LIMIT = 2;

	// which of the two limits we will hit (or have fit) first
	private int mCurrentLowerLimit = UNKNOWN_LIMIT;

	private File mSDCardDirectory;

	// State for tracking file size of recording.
	private File mRecordingFile;
	private long mMaxBytes;

	// Rate at which the file grows
	private int mBytesPerSecond;

	// time at which number of free blocks last changed
	private long mBlocksChangedTime;
	// number of available blocks at that time
	private long mLastBlocks;

	// time at which the size of the file has last changed
	private long mFileSizeChangedTime;
	// size of the file at that time
	private long mLastFileSize;


	public RemainingTimeCalculator() {
		mSDCardDirectory = Environment.getExternalStorageDirectory();
	}

	/**
	 * If called, the calculator will return the minimum of two estimates: how
	 * long until we run out of disk space and how long until the file reaches
	 * the specified size.
	 * 
	 * @param file
	 *            the file to watch
	 * @param maxBytes
	 *            the limit
	 */

	public void setFileSizeLimit(File file, long maxBytes) {
		mRecordingFile = file;
		mMaxBytes = maxBytes;
		if (DEB)
			Log.d(TAG, "file:" + mRecordingFile.getAbsolutePath()
					+ " mMaxBytes: " + mMaxBytes);
	}

	/**
	 * Resets the interpolation.
	 */
	public void reset() {
		mCurrentLowerLimit = UNKNOWN_LIMIT;
		mBlocksChangedTime = -1;
		mFileSizeChangedTime = -1;
	}

	/**
	 * Returns how long (in seconds) we can continue recording.
	 */
	public long timeRemaining() {
		// Calculate how long we can record based on free disk space
		StatFs fs = new StatFs(mSDCardDirectory.getAbsolutePath());
		long blocks = fs.getAvailableBlocks() - 10;
		long blockSize = fs.getBlockSize();
		long now = System.currentTimeMillis();

		if (DEB)
			Log.d(TAG, "AvailableBlocks: " + blocks);

		if (mBlocksChangedTime == -1 || blocks != mLastBlocks) {
			mBlocksChangedTime = now;
			mLastBlocks = blocks;
		}

		if (mLastBlocks <= 0) {
			if (DEB)
				Log.d(TAG, "zero, done, exit!!");
			return 0;
		}
		/*
		 * The calculation below always leaves one free block, since free space
		 * in the block we're currently writing to is not added. This last block
		 * might get nibbled when we close and flush the file, but we won't run
		 * out of disk.
		 */

		// at mBlocksChangedTime we had this much time
		long result = mLastBlocks * blockSize / mBytesPerSecond;
		// so now we have this much time
		result -= (now - mBlocksChangedTime) / 1000;

		if (mRecordingFile == null) {
			mCurrentLowerLimit = DISK_SPACE_LIMIT;
			return result;
		}

		// If we have a recording file set, we calculate a second estimate
		// based on how long it will take us to reach mMaxBytes.

		mRecordingFile = new File(mRecordingFile.getAbsolutePath());
		long fileSize = mRecordingFile.length();
		if (mFileSizeChangedTime == -1 || fileSize != mLastFileSize) {
			mFileSizeChangedTime = now;
			mLastFileSize = fileSize;
		}

		long result2 = (mMaxBytes - fileSize) / mBytesPerSecond;
		result2 -= (now - mFileSizeChangedTime) / 1000;
		result2 -= 1; // just for safety

		mCurrentLowerLimit = result < result2 ? DISK_SPACE_LIMIT
				: FILE_SIZE_LIMIT;

		return Math.min(result, result2);
	}

	/**
	 * Indicates which limit we will hit (or have hit) first, by returning one
	 * of FILE_SIZE_LIMIT or DISK_SPACE_LIMIT or UNKNOWN_LIMIT. We need this to
	 * display the correct message to the user when we hit one of the limits.
	 */
	public int currentLowerLimit() {
		return mCurrentLowerLimit;
	}

	/**
	 * Is there any point of trying to start recording?
	 */
	public boolean diskSpaceAvailable() {
		StatFs fs = new StatFs(mSDCardDirectory.getAbsolutePath());
		// keep one free block
		return fs.getAvailableBlocks() > 1;
	}

	/**
	 * Sets the bit rate used in the interpolation.
	 * 
	 * @param bitRate
	 *            the bit rate to set in bits/sec.
	 */
	public void setBitRate(int bitRate) {
		mBytesPerSecond = bitRate / 8;
	}
}

public class SoundRecorder extends RightScrollActivity implements
		Button.OnClickListener, Recorder.OnStateChangedListener {
	static final String TAG = "SoundRecorder";
	static final String STATE_FILE_NAME = "soundrecorder.state";
	static final String RECORDER_STATE_KEY = "recorder_state";
	static final String SAMPLE_INTERRUPTED_KEY = "sample_interrupted";
	static final String MAX_FILE_SIZE_KEY = "max_file_size";

	static final String AUDIO_3GPP = "audio/3gpp";
	static final String AUDIO_AMR = "audio/amr";
	static final String AUDIO_ANY = "audio/*";
	static final String ANY_ANY = "*/*";

	/**
     * 播放录音时的广播ACTION
     */
    private static final String RECORDER_STOP_MUSIC = "com.ingenic.amazingrecorder.start";

    /**
     * 停止播放录音时的广播ACTION
     */
    private static final String RECORDER_STOP_MUSIC_START = "com.ingenic.amazingrecorder.stop";

	private boolean isRecording = false;
	private boolean isPlaying = false;
	private static final boolean DEB = false;

	RemainingTimeCalculator mRemainingTimeCalculator;
	/*
	 * The original bit rate value is set up a small lead to the calculation of
	 * time remaining errors, the initial value of 5900
	 */
	static long firstTime = 0;
	static final int BITRATE_AMR = 12857; // bits/sec
	static final int BITRATE_3GPP = 12857;

	WakeLock mWakeLock;
	String mRequestedType = AUDIO_ANY;
	Recorder mRecorder;
	boolean mSampleInterrupted = false;
	String mErrorUiMessage = null; // Some error messages are displayed in the
	// UI,
	// not a dialog. This happens when a
	// recording
	// is interrupted for some reason.

	long mMaxFileSize = -1; // can be specified in the intent

	String mTimerFormat;
	final Handler mHandler = new Handler();
	Runnable mUpdateTimer = new Runnable() {
		public void run() {

			updateTimerView();
			if (progress < -1) {
				progress = 0;
			}

			if (mRecorder.state() != Recorder.RECORDING_STATE
					&& mRecorder.state() != Recorder.PLAYING_STATE) {
				progress = 0;
			}
			mRoundProgressBar.setProgress(progress);
			if (progress == 60) {
				progress = 0;
			}
		}
	};
	ImageButton mRecordButton;
	ImageButton mPlayButton;
	// ImageButton mStopButton;
	TextView mStateMessage1;
	TextView mStateMessage2;
	TextView mTimerView;

	// LinearLayout mExitButtons;
	ImageButton mAcceptButton;
	// Button mDiscardButton;
	private BroadcastReceiver mSDCardMountEventReceiver = null;

	// this is unused see line 900 for notes
	VUMeter mVUMeter;
	// 用于右滑
	private RightScrollView mView;
	private int progress = 0;
	private RoundProgressBar mRoundProgressBar;

	@Override
	public void onCreate(Bundle icycle) {
		super.onCreate(icycle);

		mView = getRightScrollView();

		// 初始不允许滑动
		mView.disableRightScroll();

		Intent i = getIntent();
		if (i != null) {
			String s = i.getType();
			if (AUDIO_AMR.equals(s) || AUDIO_3GPP.equals(s)
					|| AUDIO_ANY.equals(s) || ANY_ANY.equals(s)) {
				mRequestedType = s;
			} else if (s != null) {
				// we only support amr and 3gpp formats right now
				setResult(RESULT_CANCELED);
				finish();
				return;
			}

			final String EXTRA_MAX_BYTES = android.provider.MediaStore.Audio.Media.EXTRA_MAX_BYTES;
			mMaxFileSize = i.getLongExtra(EXTRA_MAX_BYTES, -1);
		}

		if (AUDIO_ANY.equals(mRequestedType) || ANY_ANY.equals(mRequestedType)) {
			// mRequestedType = AUDIO_3GPP;
			mRequestedType = AUDIO_AMR;
		}

		if (HardwareList.IsCircularScreen()) {
			mView.setContentView(R.layout.main_round);
		} else {
			mView.setContentView(R.layout.main_square);
		}

		mRoundProgressBar = (RoundProgressBar) findViewById(R.id.roundProgressBar2);

		mRecorder = new Recorder();
		mRecorder.setOnStateChangedListener(this);
		mRemainingTimeCalculator = new RemainingTimeCalculator();

		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK,
				"SoundRecorder");

		initResourceRefs();

		setResult(RESULT_CANCELED);
		registerExternalStorageListener();
		if (icycle != null) {
			Bundle recorderState = icycle.getBundle(RECORDER_STATE_KEY);
			if (recorderState != null) {
				mRecorder.restoreState(recorderState);
				mSampleInterrupted = recorderState.getBoolean(
						SAMPLE_INTERRUPTED_KEY, false);
				mMaxFileSize = recorderState.getLong(MAX_FILE_SIZE_KEY, -1);
			}
		}

		updateUi();
	}

	@Override
	protected void onResume() {
		super.onResume();
		// 窗口创建好后允许滑动
		mView.enableRightScroll();
	}

	private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals(Intent.ACTION_BATTERY_LOW)) {
				System.out.println("Intent.ACTION_BATTERY_LOW");
				mRecorder.stop();
				saveSample();
				finish();
			}
		}
	};

	// @Override
	protected void onStart() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_BATTERY_LOW);
		registerReceiver(mBroadcastReceiver, filter);
		super.onStart();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		if (HardwareList.IsCircularScreen()) {
			setContentView(R.layout.main_round);
		} else {
			setContentView(R.layout.main_square);
		}
		initResourceRefs();
		updateUi();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		if (mRecorder.sampleLength() == 0)
			return;

		Bundle recorderState = new Bundle();

		mRecorder.saveState(recorderState);
		recorderState.putBoolean(SAMPLE_INTERRUPTED_KEY, mSampleInterrupted);
		recorderState.putLong(MAX_FILE_SIZE_KEY, mMaxFileSize);

		outState.putBundle(RECORDER_STATE_KEY, recorderState);
	}

	/*
	 * Whenever the UI is re-created (due f.ex. to orientation change) we have
	 * to reinitialize references to the views.
	 */
	private void initResourceRefs() {
		mRecordButton = (ImageButton) findViewById(R.id.recordButton);
		mPlayButton = (ImageButton) findViewById(R.id.playButton);
		mAcceptButton = (ImageButton) findViewById(R.id.acceptButton);
		mStateMessage1 = (TextView) findViewById(R.id.stateMessage1);
		mStateMessage2 = (TextView) findViewById(R.id.stateMessage2);
		mTimerView = (TextView) findViewById(R.id.timerView);

		// see LINE 900 for notes
		// mVUMeter = (VUMeter) findViewById(R.id.uvMeter);

		mRecordButton.setOnClickListener(this);
		mPlayButton.setOnClickListener(this);
		mAcceptButton.setOnClickListener(this);

		mTimerFormat = getResources().getString(R.string.timer_format);
		// see LINE 900 for notes
		// mVUMeter.setRecorder(mRecorder);
	}

	/*
	 * Make sure we're not recording music playing in the background, ask the
	 * MediaPlaybackService to pause playback.
	 */
	private void stopAudioPlayback() {
		// Shamelessly copied from MediaPlaybackService.java, which
		// should be public, but isn't.
		Intent i = new Intent("com.android.music.musicservicecommand");
		i.putExtra("command", "pause");

		sendBroadcast(i);
	}

	/*
	 * Handle the buttons.
	 */
	public void onClick(View button) {
		if (!button.isEnabled())
			return;

		switch (button.getId()) {
		case R.id.recordButton:
			if (isRecording == false) {
				mRemainingTimeCalculator.reset();
				if (!Environment.getExternalStorageState().equals(
						Environment.MEDIA_MOUNTED)) {
					mSampleInterrupted = true;
					mErrorUiMessage = getResources().getString(
							R.string.insert_sd_card);
					updateUi();
				} else if (!mRemainingTimeCalculator.diskSpaceAvailable()) {
					mSampleInterrupted = true;
					mErrorUiMessage = getResources().getString(
							R.string.storage_is_full);
					updateUi();
				} else {
					stopAudioPlayback();

					if (AUDIO_AMR.equals(mRequestedType)) {
						mRemainingTimeCalculator.setBitRate(BITRATE_AMR);
						mRecorder
								.startRecording(
										MediaRecorder.OutputFormat.AMR_NB,
										".amr", this);
					} else if (AUDIO_3GPP.equals(mRequestedType)) {
						mRemainingTimeCalculator.setBitRate(BITRATE_3GPP);
						mRecorder.startRecording(
								MediaRecorder.OutputFormat.THREE_GPP, ".3gpp",
								this);
					} else {
						throw new IllegalArgumentException(
								"Invalid output file type requested");
					}

					if (mMaxFileSize != -1) {
						mRemainingTimeCalculator.setFileSizeLimit(
								mRecorder.sampleFile(), mMaxFileSize);
					}
				}
			} else {
				mRecorder.stopRecording();
				mRecorder.stop();
			}
			break;
		case R.id.playButton:
			if (isPlaying == false) {
				mRecorder.startPlayback();
			} else {
				mRecorder.stopPlayback();
				mRecorder.stop();
			}

			break;
		case R.id.acceptButton:
			mRecorder.stop();
			saveSample();
			finish();
			break;
		/*
		 * case R.id.discardButton: mRecorder.delete(); finish(); break;
		 */
		}
	}

	/*
	 * Handle the "back" hardware key.
	 */
	@Override
	public void onBackPressed() {
		super.onBackPressed();

		switch (mRecorder.state()) {
		case Recorder.IDLE_STATE:
			mRecorder.delete();
			break;
		case Recorder.PLAYING_STATE:
			mRecorder.stop();
			// saveSample();
			break;
		case Recorder.RECORDING_STATE:
			mRecorder.delete();
			break;
		}

		SoundRecorder.this.finish();
		// dialog();
	}

	@Override
	public void onStop() {
		super.onStop();
		mRecorder.stop();
		unregisterReceiver(mBroadcastReceiver);
	}

	@Override
	protected void onPause() {
		super.onPause();
		mSampleInterrupted = mRecorder.state() == Recorder.RECORDING_STATE;
		mRecorder.stop();

		if (mWakeLock != null && mWakeLock.isHeld()) {
			mWakeLock.release();
			IwdsLog.d(TAG, "WakeLock is released!");
		}
	}

	protected void dialog() {
		AlertDialog.Builder builder = new Builder(SoundRecorder.this);
		builder.setMessage(getString(R.string.insuretoexit));
		builder.setTitle(getString(R.string.notice));
		builder.setPositiveButton(getString(R.string.positive),
				new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						SoundRecorder.this.finish();
					}
				});
		builder.setNegativeButton(getString(R.string.negative),
				new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
		builder.create().show();
	}

	public File renameFile(File f) {
		String filename = f.getName();
		int index = -1;
		File f_new = null;
		String str = null;
		String str_path = null;
		Date d = new Date();
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMdd hh-mm-ss");
		str = dateformat.format(d);
		File sampleDir = Environment.getExternalStorageDirectory();
		if (!sampleDir.canWrite()) {
			sampleDir = new File("/sdcard/sdcard");
		}
		str_path = sampleDir + "/recorder";
		sampleDir = new File(str_path);
		if (!sampleDir.exists()) {
			sampleDir.mkdir();
		}
		index = filename.lastIndexOf(".");
		if (index != -1) {
			filename = filename.substring(index);
		}
		f_new = new File(sampleDir, "rec_" + str + filename);
		f.renameTo(f_new);
		return f_new;
	}

	/*
	 * If we have just recorded a smaple, this adds it to the media data base
	 * and sets the result to the sample's URI.
	 */
	private void saveSample() {
		if (mRecorder.sampleLength() == 0)
			return;
		Uri uri = null;
		try {
			File file = renameFile(mRecorder.sampleFile());
			uri = this.addToMediaDB(file);
			// uri = this.addToMediaDB(mRecorder.sampleFile());
		} catch (UnsupportedOperationException ex) { // Database manipulation
			// failure
			return;
		}
		if (uri == null) {
			return;
		}
		setResult(RESULT_OK, new Intent().setData(uri));
	}

	/*
	 * Called on destroy to unregister the SD card mount event receiver.
	 */
	@Override
	public void onDestroy() {
		if (mSDCardMountEventReceiver != null) {
			unregisterReceiver(mSDCardMountEventReceiver);
			mSDCardMountEventReceiver = null;
		}
		super.onDestroy();
	}

	/*
	 * Registers an intent to listen for ACTION_MEDIA_EJECT/ACTION_MEDIA_MOUNTED
	 * notifications.
	 */
	private void registerExternalStorageListener() {
		if (mSDCardMountEventReceiver == null) {
			mSDCardMountEventReceiver = new BroadcastReceiver() {
				@Override
				public void onReceive(Context context, Intent intent) {
					String action = intent.getAction();
					if (action.equals(Intent.ACTION_MEDIA_EJECT)) {
						mRecorder.delete();
					} else if (action.equals(Intent.ACTION_MEDIA_MOUNTED)) {
						mSampleInterrupted = false;
						updateUi();
					}
				}
			};
			IntentFilter iFilter = new IntentFilter();
			iFilter.addAction(Intent.ACTION_MEDIA_EJECT);
			iFilter.addAction(Intent.ACTION_MEDIA_MOUNTED);
			iFilter.addDataScheme("file");
			registerReceiver(mSDCardMountEventReceiver, iFilter);
		}
	}

	/*
	 * A simple utility to do a query into the databases.
	 */
	private Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		try {
			ContentResolver resolver = getContentResolver();
			if (resolver == null) {
				return null;
			}
			return resolver.query(uri, projection, selection, selectionArgs,
					sortOrder);
		} catch (UnsupportedOperationException ex) {
			return null;
		}
	}

	/*
	 * Add the given audioId to the playlist with the given playlistId; and
	 * maintain the play_order in the playlist.
	 */
	private void addToPlaylist(ContentResolver resolver, int audioId,
			long playlistId) {
		String[] cols = new String[] { "count(*)" };

		Uri uri = MediaStore.Audio.Playlists.Members.getContentUri("external",
				playlistId);
		Cursor cur = resolver.query(uri, cols, null, null, null);
		cur.moveToFirst();
		final int base = cur.getInt(0);
		cur.close();
		ContentValues values = new ContentValues();
		values.put(MediaStore.Audio.Playlists.Members.PLAY_ORDER,
				Integer.valueOf(base + audioId));
		values.put(MediaStore.Audio.Playlists.Members.AUDIO_ID, audioId);
		resolver.insert(uri, values);

	}

	/*
	 * Obtain the id for the default play list from the audio_playlists table.
	 */
	private int getPlaylistId(Resources res) {
		Uri uri = MediaStore.Audio.Playlists.getContentUri("external");
		final String[] ids = new String[] { MediaStore.Audio.Playlists._ID };
		final String where = MediaStore.Audio.Playlists.NAME + "=?";
		final String[] args = new String[] { res
				.getString(R.string.audio_db_playlist_name) };
		Cursor cursor = query(uri, ids, where, args, null);
		if (cursor == null) {
			Log.v(TAG, "query returns null");
		}
		int id = -1;
		if (cursor != null) {
			cursor.moveToFirst();
			if (!cursor.isAfterLast()) {
				id = cursor.getInt(0);
			}
		}
		cursor.close();
		return id;
	}

	/*
	 * Create a playlist with the given default playlist name, if no such
	 * playlist exists.
	 */
	private Uri createPlaylist(Resources res, ContentResolver resolver) {
		ContentValues cv = new ContentValues();
		cv.put(MediaStore.Audio.Playlists.NAME,
				res.getString(R.string.audio_db_playlist_name));
		Uri uri = resolver.insert(
				MediaStore.Audio.Playlists.getContentUri("external"), cv);
		if (uri == null) {
			// new AlertDialog.Builder(this).setTitle(R.string.app_name)
			// .setMessage(R.string.error_mediadb_new_record)
			// .setPositiveButton(R.string.button_ok, null)
			// .setCancelable(false).show();
			AmazingToast.showToast(getApplicationContext(), getResources()
					.getString(R.string.error_mediadb_new_record),
					AmazingToast.LENGTH_SHORT);
		}
		return uri;
	}

	/*
	 * Adds file and returns content uri.
	 */
	private Uri addToMediaDB(File file) {
		Resources res = getResources();
		ContentValues cv = new ContentValues();
		long current = System.currentTimeMillis();
		long modDate = file.lastModified();
		// Date date = new Date(current);
		// SimpleDateFormat formatter = new SimpleDateFormat(
		// res.getString(R.string.audio_db_title_format));
		// String title = formatter.format(date);
		String title = file.getName();
		int index = title.lastIndexOf(".");
		title = title.substring(0, index);
		long sampleLengthMillis = mRecorder.sampleLength() * 1000L;

		// Lets label the recorded audio file as NON-MUSIC so that the file
		// won't be displayed automatically, except for in the playlist.
		cv.put(MediaStore.Audio.Media.IS_MUSIC, "1");

		cv.put(MediaStore.Audio.Media.TITLE, title);
		cv.put(MediaStore.Audio.Media.DATA, file.getAbsolutePath());
		cv.put(MediaStore.Audio.Media.DATE_ADDED, (int) (current / 1000));
		cv.put(MediaStore.Audio.Media.DATE_MODIFIED, (int) (modDate / 1000));
		cv.put(MediaStore.Audio.Media.DURATION, sampleLengthMillis);
		cv.put(MediaStore.Audio.Media.MIME_TYPE, mRequestedType);
		cv.put(MediaStore.Audio.Media.ARTIST,
				res.getString(R.string.audio_db_artist_name));
		cv.put(MediaStore.Audio.Media.ALBUM,
				res.getString(R.string.audio_db_album_name));
		Log.d(TAG, "Inserting audio record: " + cv.toString());
		ContentResolver resolver = getContentResolver();
		Uri base = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
		Log.d(TAG, "ContentURI: " + base);
		Uri result = resolver.insert(base, cv);
		if (result == null) {
			new AlertDialog.Builder(this).setTitle(R.string.app_name)
					.setMessage(R.string.error_mediadb_new_record)
					.setPositiveButton(R.string.button_ok, null)
					.setCancelable(false).show();
			return null;
		}
		if (getPlaylistId(res) == -1) {
			createPlaylist(res, resolver);
		}
		int audioId = Integer.valueOf(result.getLastPathSegment());
		addToPlaylist(resolver, audioId, getPlaylistId(res));

		// Notify those applications such as Music listening to the
		// scanner events that a recorded audio file just created.
		sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, result));
		return result;
	}

	// this function is called when the recording use for more than 4 hours
	public void startRecord() {
		mRemainingTimeCalculator.reset();
		if (!Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {
			mSampleInterrupted = true;
			mErrorUiMessage = getResources().getString(R.string.insert_sd_card);
			updateUi();
		} else if (!mRemainingTimeCalculator.diskSpaceAvailable()) {
			mSampleInterrupted = true;
			mErrorUiMessage = getResources()
					.getString(R.string.storage_is_full);
			updateUi();
		} else {
			stopAudioPlayback();

			if (AUDIO_AMR.equals(mRequestedType)) {
				mRemainingTimeCalculator.setBitRate(BITRATE_AMR);
				mRecorder.startRecording(MediaRecorder.OutputFormat.AMR_NB,
						".amr", this);
			} else if (AUDIO_3GPP.equals(mRequestedType)) {
				mRemainingTimeCalculator.setBitRate(BITRATE_3GPP);
				mRecorder.startRecording(MediaRecorder.OutputFormat.THREE_GPP,
						".3gpp", this);
			} else {
				throw new IllegalArgumentException(
						"Invalid output file type requested");
			}
			if (mMaxFileSize != -1) {
				mRemainingTimeCalculator.setFileSizeLimit(
						mRecorder.sampleFile(), mMaxFileSize);
			}
		}
	}

	/**
	 * Update the big MM:SS timer. If we are in playback, also update the
	 * progress bar.
	 */
	private void updateTimerView() {
		int state = mRecorder.state();

		boolean ongoing = state == Recorder.RECORDING_STATE
				|| state == Recorder.PLAYING_STATE;

		long time = ongoing ? mRecorder.progress() : mRecorder.sampleLength();
		String timeStr = String.format(mTimerFormat, time / 60, time % 60);
		mTimerView.setText(timeStr);
		if(time==0) progress=0;
		else{
		    progress = (int) (time % 60);
		    if(progress==0) progress=60;
		}

		if (state == Recorder.PLAYING_STATE) {
		} else if (state == Recorder.RECORDING_STATE) {
			if (time != 0 && time % 14400 == 0) {
				// when the recording is used too long for 4 hours, it will save
				// the data and reset
				mRecorder.clear();
				saveSample();
				startRecord();
			}
			updateTimeRemaining();
		}

		if (ongoing) {
			mHandler.postDelayed(mUpdateTimer, 1000);
		}
	}

	/*
	 * Called when we're in recording state. Find out how much longer we can go
	 * on recording. If it's under 5 minutes, we display a count-down in the UI.
	 * If we've run out of time, stop the recording.
	 */
	private void updateTimeRemaining() {
		long t = mRemainingTimeCalculator.timeRemaining();

		if (DEB)
			Log.w(TAG, "time left: " + t);
		if (t <= 0) {
			mSampleInterrupted = true;

			int limit = mRemainingTimeCalculator.currentLowerLimit();
			switch (limit) {
			case RemainingTimeCalculator.DISK_SPACE_LIMIT:
				mErrorUiMessage = getResources().getString(
						R.string.storage_is_full);
				break;
			case RemainingTimeCalculator.FILE_SIZE_LIMIT:
				mErrorUiMessage = getResources().getString(
						R.string.max_length_reached);
				break;
			default:
				mErrorUiMessage = null;
				break;
			}
			Toast.makeText(SoundRecorder.this,
					getResources().getString(R.string.storage_is_full),
					Toast.LENGTH_SHORT).show();
			mRecorder.stop();
			return;
		}

		Resources res = getResources();
		String timeStr = "";

		if (t < 60)
			timeStr = String.format(res.getString(R.string.sec_available), t);
		else if (t < 540)
			timeStr = String.format(res.getString(R.string.min_available),
					t / 60 + 1);
		mStateMessage1.setText(timeStr);
	}

	/**
	 * Shows/hides the appropriate child views for the new state.
	 */
	private void updateUi() {
		Resources res = getResources();

		switch (mRecorder.state()) {
		case Recorder.IDLE_STATE:
			isPlaying = false;
			isRecording = false;
			mRoundProgressBar.setProgress(0);
			mRecordButton.setImageResource(R.drawable.newrecord);
			mRecordButton.setEnabled(true);
			mRecordButton.setFocusable(true);
			if (mRecorder.sampleLength() == 0) {
				mPlayButton.setEnabled(false);
				mPlayButton.setFocusable(false);
				mAcceptButton.setEnabled(false);
				mAcceptButton.setFocusable(false);
				mRecordButton.requestFocus();
				mStateMessage1.setVisibility(View.INVISIBLE);
				mStateMessage2.setVisibility(View.INVISIBLE);
				setTitle(res.getString(R.string.record_your_message));
			} else {
				mPlayButton.setImageResource(R.drawable.btn_play);
				mPlayButton.setEnabled(true);
				mPlayButton.setFocusable(true);
				mAcceptButton.setEnabled(true);
				mAcceptButton.setFocusable(true);
				mTimerView.setVisibility(View.VISIBLE);
				mStateMessage1.setVisibility(View.INVISIBLE);
				mStateMessage2.setVisibility(View.INVISIBLE);
				setTitle(res.getString(R.string.message_recorded));
			}

			if (mSampleInterrupted) {
				mStateMessage2.setVisibility(View.VISIBLE);
				mStateMessage2.setText(res
						.getString(R.string.recording_stopped));
			}

			if (mErrorUiMessage != null) {
				mStateMessage1.setText(mErrorUiMessage);
				mStateMessage1.setVisibility(View.VISIBLE);
			}
          sendBroadcast(new Intent(RECORDER_STOP_MUSIC_START));
			break;
		case Recorder.RECORDING_STATE:
			isRecording = true;
			isPlaying = false;
			mRecordButton.setImageResource(R.drawable.pause);
			mPlayButton.setImageResource(R.drawable.btn_play);
			mPlayButton.setEnabled(true);
			mPlayButton.setFocusable(true);
			mAcceptButton.setEnabled(true);
			mAcceptButton.setFocusable(true);
			mStateMessage1.setVisibility(View.VISIBLE);
			mStateMessage2.setVisibility(View.VISIBLE);
			mStateMessage2.setText(res.getString(R.string.recording));
			setTitle(res.getString(R.string.record_your_message));
			break;

		case Recorder.PLAYING_STATE:
			isPlaying = true;
			isRecording = false;
			if (mRecorder.sampleLength() == 0) {
				break;
			}
			mPlayButton.setImageResource(R.drawable.pause);
			mRecordButton.setImageResource(R.drawable.newrecord);
			mAcceptButton.setEnabled(true);
			mAcceptButton.setFocusable(true);
			mStateMessage1.setVisibility(View.INVISIBLE);
			mStateMessage2.setVisibility(View.VISIBLE);
			mStateMessage2.setText(R.string.playing);
			setTitle(res.getString(R.string.review_message));
			sendBroadcast(new Intent(RECORDER_STOP_MUSIC));
			break;
		}

		updateTimerView();
		// delete this code for cutting down the power comsuption,if reable
		// it, the imageviews must
		// be reable,but them has been destoried to ashed, u have to rewrite
		// them.
		// mVUMeter.invalidate();
	}

	/*
	 * Called when Recorder changed it's state.
	 */
	@Override
	public void onStateChanged(int state) {
		if (state == Recorder.PLAYING_STATE
				|| state == Recorder.RECORDING_STATE) {
			mSampleInterrupted = false;
			mErrorUiMessage = null;
			mWakeLock.acquire(); // we don't want to go to sleep while recording
			// or playing
		} else {
			if (mWakeLock != null && mWakeLock.isHeld()) {
				mWakeLock.release();
				IwdsLog.d(TAG, "WakeLock is released!");
			}
		}
		updateUi();
	}

	/*
	 * Called when MediaPlayer encounters an error.
	 */
	public void onError(int error) {
		Resources res = getResources();

		String message = null;
		switch (error) {
		case Recorder.SDCARD_ACCESS_ERROR:
			message = res.getString(R.string.error_sdcard_access);
			break;
		case Recorder.IN_CALL_RECORD_ERROR:
			// TODO: update error message to reflect that the recording could
			// not be performed during a call.
		case Recorder.INTERNAL_ERROR:
			message = res.getString(R.string.error_app_internal);
			break;
		}
		if (message != null) {
			// new AlertDialog.Builder(this).setTitle(R.string.app_name)
			// .setMessage(message)
			// .setPositiveButton(R.string.button_ok, null)
			// .setCancelable(false).show();
			AmazingToast.showToast(getApplicationContext(), message,
					AmazingToast.LENGTH_SHORT);
		}
	}

	@Override
	public void onRightScroll() {
		onBackPressed();
	}

}
